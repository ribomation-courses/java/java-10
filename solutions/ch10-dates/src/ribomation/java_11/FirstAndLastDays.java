package ribomation.java_11;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.AbstractMap;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class FirstAndLastDays {
    public static void main(String[] args) {
        FirstAndLastDays app = new FirstAndLastDays();

        System.out.println("--- LocalDate ---");
        app.run_using_LocalDate();

        System.out.println("--- Date ---");
        app.run_using_Date();
    }

    void run_using_LocalDate() {
        run_using_LocalDate(LocalDate.now());
        run_using_LocalDate(LocalDate.of(2017, Month.MAY, 2));
        run_using_LocalDate(LocalDate.of(2017, Month.FEBRUARY, 15));
        run_using_LocalDate(LocalDate.of(2016, Month.FEBRUARY, 15));
    }

    void run_using_Date() {
        run_using_Date(Calendar.getInstance().getTime());
        run_using_Date(new Calendar.Builder().setDate(2017, (5 - 1), 2).build().getTime());
        run_using_Date(new Calendar.Builder().setDate(2017, (2 - 1), 15).build().getTime());
        run_using_Date(new Calendar.Builder().setDate(2016, (2 - 1), 15).build().getTime());
    }

    // --- New API ---
    void run_using_LocalDate(LocalDate date) {
        Map.Entry<LocalDate, LocalDate> result = firstAndLastOf(date);
        System.out.printf("%s --> %s%n", date, result.toString().replace('=', ':'));
    }

    Map.Entry<LocalDate, LocalDate> firstAndLastOf(LocalDate date) {
        LocalDate first = date.with(TemporalAdjusters.firstDayOfMonth());
        LocalDate last  = date.with(TemporalAdjusters.lastDayOfMonth());
        return new AbstractMap.SimpleEntry<>(first, last);
    }


    // --- Old API ---
    void run_using_Date(Date date) {
        Map.Entry<Date, Date> result = firstAndLastOf(date);
        SimpleDateFormat      fmt    = new SimpleDateFormat("yyyy-MM-dd");
        System.out.printf("%s --> %s:%s%n",
                fmt.format(date),
                fmt.format(result.getKey()),
                fmt.format(result.getValue()));
    }

    Map.Entry<Date, Date> firstAndLastOf(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);

        //clear time part
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        //compute first of month
        c.set(Calendar.DAY_OF_MONTH, 1);
        Date first = c.getTime();

        //compute last of month
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date last = c.getTime();

        return new AbstractMap.SimpleEntry<>(first, last);
    }

}
