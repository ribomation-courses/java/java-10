package ribomation;
import java.util.ArrayList;
import java.util.Optional;

public class OptList<T> extends ArrayList<T> {
    public Optional<T> lookup(int idx) {
        try {
            return Optional.ofNullable(super.get(idx));
        } catch (IndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }
}
