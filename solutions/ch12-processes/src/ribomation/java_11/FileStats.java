package ribomation.java_11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileStats {
    public static void main(String[] args) throws Exception {
        FileStats app = new FileStats();
        app.run(Paths.get("../../"), 12);
    }

    private void run(Path dir, int count) throws IOException {
        System.out.printf("Processing dir: %s ...%n", dir);

        List<Process> pipeline = ProcessBuilder.startPipeline(List.of(
                new ProcessBuilder("find",
                        dir.toAbsolutePath().normalize().toString(),
                        "-type", "f",
                        "-name", "*.java"),
                new ProcessBuilder("xargs", "wc"),
                new ProcessBuilder("grep", "-v", "total"),
                new ProcessBuilder("sort", "-nr", "-k", "2"),
                new ProcessBuilder("head", "-" + count)
        ));

        Process last = pipeline.get(pipeline.size() - 1);
        if (last.toHandle().isAlive()) {
            BufferedReader in = new BufferedReader(new InputStreamReader(last.getInputStream()));
            try (in) {
                in.lines()
                        .map(line -> line.replaceFirst("/mnt.*gitlab/", ""))
                        .forEach(System.out::println);
            }
        }
    }
}
