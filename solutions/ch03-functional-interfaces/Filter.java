package ribomation.java_11.fun_interfaces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Filter {
    public static void main(String[] args) {
        Filter app = new Filter();
        app.run_with_anon_class();
        app.run_with_lambda();
        app.run_with_method_ref();
    }
	
	void run(Predicate<String> p) {
        List<String> strs = Arrays.asList("hej", "", "hi", "", "");
        System.out.printf("before: %s (%s)%n", strs, p.getClass());
        List<String> res = filter(strs, p);
        System.out.printf("after: %s%n", res);
    }

    List<String> filter(List<String> lst, Predicate<String> p) {
        List<String> result = new ArrayList<>();
        for (String s : lst) if (p.negate().test(s)) result.add(s);
        return result;
    }

	
    void run_with_anon_class() {
        run(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.isEmpty();
            }
        });
    }

    void run_with_lambda() {
        run(s -> s.isEmpty());
    }

    void run_with_method_ref() {
        run(String::isEmpty);
    }

    
}
