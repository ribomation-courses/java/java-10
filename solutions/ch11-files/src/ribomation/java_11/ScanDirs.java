package ribomation.java_11;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ScanDirs {
    public static void main(String[] args) throws Exception {
        String dirName = "../../../../../";
        String phrase  = "Reader";

        Path dir          = Paths.get(dirName);
        if (!Files.isDirectory(dir)) {
            throw new IllegalArgumentException("not a directory: " + dir.toString());
        }

        System.out.printf("Scanning %s ...%n", dir.toAbsolutePath().normalize());
        long totalFileSize = new ScanDirs().scan(dir, phrase);
        System.out.printf("Total size of DIR '%s' is %.2f KB", dir, totalFileSize / 1024D);
    }

    long scan(Path dir, String phrase) throws IOException {
        return Files.walk(dir)
                .filter(p -> Files.isRegularFile(p))
                .filter(p -> p.toString().endsWith(".java"))
                .filter(p -> p.toFile().length() > 512)
                .filter(p -> contains(p, phrase))
                .peek(System.out::println)
                .mapToLong(p -> p.toFile().length())
                .sum()
                ;
    }

    boolean contains(Path p, String phrase) {
        final String PHRASE = phrase.toLowerCase();
        try {
            return Files.lines(p)
                    .map(String::toLowerCase)
                    .anyMatch(line -> line.contains(PHRASE));
        } catch (Exception ignore) { }
        return false;
    }

    long sizeOf(Path p) {
        try { return Files.size(p) ; } //ALT) p.toFile().length()
        catch (IOException ignore) {  }
        return 0;
    }
}
