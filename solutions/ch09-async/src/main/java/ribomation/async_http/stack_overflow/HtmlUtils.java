package ribomation.async_http.stack_overflow;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Various HTML and string methods
 */
public class HtmlUtils {
    public static String H(int level, String text) {
        return String.format("<h%d class=\"bg-success text-white p-1\">Language: %s</h%d>%n",
                level, capitalize(text), level);
    }

    public static String UL(List<String> items) {
        return String.format("<ul>%n%s%n</ul>%n", items
                .stream()
                .map((String item) -> String.format("<li>%s</li>", esc(item)))
                .collect(Collectors.joining(System.lineSeparator()))
        );
    }

    public static String esc(String html) {
        return html
                .replaceAll("<", "&lt;")
                .replaceAll(">", "&gt;")
                .replaceAll("&", "&amp;")
                ;
    }

    public static String replace(String html, String key, String value) {
        return html.replaceAll("@" + key + "@", value);
    }

    public static String capitalize(String s) {
        if (isEmpty(s)) return "";
        if (s.length() == 1) return s.toUpperCase();
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public static boolean isEmpty(String s) {
        return s == null || s.trim().length() == 0;
    }
}
