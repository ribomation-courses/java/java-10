package ribomation.async_http.stack_overflow;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.*;
import java.util.stream.Collectors;

/**
 * Util class that encapsulates ugly checked exceptions
 */
public class IOUtils {

    public static BufferedReader openReader(Path path) {
        try {
            return Files.newBufferedReader(path);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Document openJsoup(String url) {
        try {
            return Jsoup.connect(url).get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String load(Path path) {
        try {
            return String.join(System.lineSeparator(), Files.readAllLines(path));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void store(Path path, String content) {
        try (BufferedWriter out = Files.newBufferedWriter(path, StandardOpenOption.CREATE))
        {
            out.write(content);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Path mkdir(Path dir) {
        try {
            return Files.createDirectories(dir);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void copy(Path file, Path todir) {
        try {
            Files.copy(file,
                    Paths.get(todir.toString(), file.getFileName().toString()),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
