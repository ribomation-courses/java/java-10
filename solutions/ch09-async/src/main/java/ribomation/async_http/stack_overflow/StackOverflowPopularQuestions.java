package ribomation.async_http.stack_overflow;

import org.jsoup.nodes.Element;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StackOverflowPopularQuestions {
    public static void main(String[] args) {
        StackOverflowPopularQuestions app = new StackOverflowPopularQuestions();
        app.run();
    }

    final String assetsDir    = "./src/assets";
    final Path   tagsFile     = Paths.get(assetsDir, "language-tags.txt");
    final Path   templateFile = Paths.get(assetsDir, "results-template.html");
    final Path   cssFile      = Paths.get(assetsDir, "bootstrap.min.css");
    final Path   jsFile       = Paths.get(assetsDir, "bootstrap.bundle.min.js");
    final Path   outDir       = Paths.get("./build/html");
    final Path   resultsFile  = Paths.get(outDir.toString(), "stack-overflow.html");
    final String baseUrl      = "https://stackoverflow.com/questions/tagged/";
    final int    maxTopics    = 8;

    void run() {
        long start = System.nanoTime();
        CompletableFuture
                .supplyAsync(() ->
                        IOUtils.openReader(tagsFile).lines()
                )
                .thenApply((Stream<String> tags) ->
                        tags.map((String tag) ->
                                CompletableFuture.supplyAsync(() -> {
                                    Result result = new Result(tag);
                                    result.topics = IOUtils
                                            .openJsoup(baseUrl + tag)
                                            .select("a.question-hyperlink")
                                            .stream()
                                            .map(Element::text)
                                            .limit(maxTopics)
                                            .collect(Collectors.toList());
                                    return result;
                                })
                        ))
                .thenApply((Stream<CompletableFuture<Result>> promises) ->
                        promises.map(CompletableFuture::join)
                )
                .thenApply((Stream<Result> results) -> results
                        .map(result ->
                                HtmlUtils.H(3, result.tag) + HtmlUtils.UL(result.topics))
                        .collect(Collectors.joining(System.lineSeparator()))
                )
                .thenApply((String body) ->
                        Stream.of(IOUtils.load(templateFile))
                                .map(html -> HtmlUtils.replace(html, "BODY", body))
                                .map(html -> HtmlUtils.replace(html, "TITLE", "StackOverflow Popular Questions"))
                                .map(html -> HtmlUtils.replace(html, "DATE", String.format("%1$tF %1$tR", new Date())))
                                .collect(Collectors.joining())
                )
                .thenApply((String html) -> {
                    IOUtils.mkdir(outDir);
                    IOUtils.store(resultsFile, html);
                    IOUtils.copy(cssFile, outDir);
                    IOUtils.copy(jsFile, outDir);
                    return System.nanoTime() - start;
                })
                .thenAccept((Long elapsedNanoSeconds) -> {
                    System.out.printf("Elapsed time: %.3f seconds%n", elapsedNanoSeconds * 1E-9);
                    System.out.printf("Results HTML: %s", resultsFile);
                })
                .exceptionally((Throwable err) -> {
                    err.printStackTrace();
                    return null;
                })
                .join();
    }

}

