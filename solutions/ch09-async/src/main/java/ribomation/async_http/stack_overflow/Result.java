package ribomation.async_http.stack_overflow;

import java.util.ArrayList;
import java.util.List;

public class Result {
    String       tag;
    List<String> topics = new ArrayList<>();

    public Result(String tag) {
        this.tag = tag;
    }
}
