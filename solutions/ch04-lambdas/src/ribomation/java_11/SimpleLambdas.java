package ribomation.java_11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.UnaryOperator;

public class SimpleLambdas {
    public static void main(String[] args) {
        SimpleLambdas app = new SimpleLambdas();
        app.runRepeat();
        app.runTransform();
    }

    interface RepeatCallback {
        void doit(int idx);
    }

    void repeat(int count, RepeatCallback expr) {
        for (int k = 1; k <= count; ++k) expr.doit(k);
    }

    void runRepeat() {
        repeat(11, k -> System.out.printf("[%d] Java 11 is cool%n", k));
    }

    <T> List<T> transform(List<T> lst, UnaryOperator<T> f) {
        List<T> result = new ArrayList<>();
        for (T x : lst) result.add(f.apply(x));
        return result;
    }

    void runTransform() {
        System.out.printf("nums: %s%n",
                transform(
                        Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
                        n -> n * n));
    }
}
