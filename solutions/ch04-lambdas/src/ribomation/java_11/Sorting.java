package ribomation.java_11;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Sorting {
    public static void main(String[] args) {
        Sorting app = new Sorting();
        app.run(Arrays.asList("hej", "hi", "hello", "ho", "tjabba"));
    }

    void run(List<String> words) {
        System.out.printf("before: %s%n", words);

        Comparator<String> DESC = (String left, String right) -> Integer.compare(right.length(), left.length());
        words.sort(DESC);
        System.out.printf("DESC : %s%n", words);

        words.sort(DESC.reversed());
        System.out.printf("ASC : %s%n", words);

        words.sort((left, right) -> Math.random() < 0.5 ? -1 : +1);
        System.out.printf("RAND: %s%n", words);

        words.sort(Comparator.comparingInt(String::length));
        System.out.printf("ASC : %s%n", words);

    }
}

