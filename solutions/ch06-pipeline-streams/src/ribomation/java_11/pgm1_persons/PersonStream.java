package ribomation.java_11.pgm1_persons;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersonStream {
    public static void main(String[] args) throws Exception {
        PersonStream app    = new PersonStream();
        app.run("./data/many-persons.csv");
    }

    void run(String path) throws IOException {
        aggregatePersons(openStream(path))
                .forEach(System.out::println);
    }

    Stream<String> openStream(String path) throws IOException {
        return Files.lines(Paths.get(path));
    }

    List<Person> aggregatePersons(Stream<String> lines) {
        return lines
                .skip(1)
                .map(csv -> csv.split(","))
                .map(fields -> new Person(fields[0], fields[1], fields[2], fields[3]))
                .filter(Person::isFemale)
                .filter(p -> 30 <= p.getAge() && p.getAge() <= 40)
                .filter(p -> p.getPostCode() < 20000)
                .collect(Collectors.toList())
                ;
    }
}
