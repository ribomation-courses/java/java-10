package ribomation.java_11.pgm1_persons;

public class Person {
    String  name;
    boolean female;
    int     age;
    int     postCode;

    //name,gender,age,postCode
    public Person(String name, String gender, String age, String postCode) {
        this(name, gender.equals("Female"), Integer.parseInt(age), Integer.parseInt(postCode));
    }

    public Person(String name, boolean female, int age, int postCode) {
        this.name = name;
        this.female = female;
        this.age = age;
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", female=" + female +
                ", postCode=" + postCode +
                '}';
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public boolean isFemale() {
        return female;
    }

    public int getPostCode() {
        return postCode;
    }
}
