package ribomation.java_11.pgm3_climate;

public class Data {
    private final int    year;
    private final double temp;

    public Data(int year, double temp) {
        this.year = year;
        this.temp = temp;
    }

    public int getYear() {
        return year;
    }

    public double getTemp() {
        return temp;
    }
}
