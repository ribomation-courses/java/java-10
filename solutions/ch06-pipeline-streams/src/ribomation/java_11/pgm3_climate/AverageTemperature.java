package ribomation.java_11.pgm3_climate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;

public class AverageTemperature {
    public static void main(String[] args) throws Exception {
        String             webFile  = "https://docs.ribomation.se/java/java-8/climate-data.txt.gz";
        String             dataFile = "..\\..\\..\\..\\..\\java8\\src\\large-files\\climate-data.txt.gz";
        AverageTemperature app      = new AverageTemperature();
        app.run(dataFile);
    }

    void run(String climateDataFile) throws IOException {
        elapsed("Climate Data", () -> {
            try {
                //var lines = openWebFile(climateDataFile);
                var lines = openFile(climateDataFile);
                var data  = aggregateClimateData(lines);
                new TreeMap<>(data)
                        .forEach((year, temp) -> System.out.printf("%d: %.2f C%n", year, temp));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    Stream<String> openFile(String path) throws IOException {
        return new BufferedReader(
                new InputStreamReader(
                        new GZIPInputStream(
                                Files.newInputStream(Paths.get(path)))))
                .lines()
                .parallel();
    }

    Stream<String> openWebFile(String url) throws IOException {
        return new BufferedReader(
                new InputStreamReader(
                        new GZIPInputStream(new URL(url).openStream())))
                .lines()
                .parallel();
    }

    Map<Integer, Double> aggregateClimateData(Stream<String> lines) {
        return lines
//                .limit(1000)
//                .peek(System.out::println)
                .filter(line -> !line.startsWith("STN--"))
                .map(line -> {
                    String[] fields = line.split("\\s+");
                    return Map.of("date", fields[2], "temp", fields[3]);
                })
                .map(data -> {
                    LocalDate localDate  = LocalDate.parse(data.get("date"), DateTimeFormatter.BASIC_ISO_DATE);
                    double    fahrenheit = Double.parseDouble(data.get("temp"));
                    double    celsius    = (5D / 9D) * (fahrenheit - 32D);
                    return new Data(localDate.getYear(), celsius);
                })
                .collect(Collectors.groupingBy(Data::getYear, Collectors.averagingDouble(Data::getTemp)));
    }

    <T> void elapsed(String name, Runnable stmts) {
        System.out.printf("[%s] running...%n", name);
        long start = System.nanoTime();
        try {
            stmts.run();
        } finally {
            long end = System.nanoTime();
            System.out.printf("[%s] elapsed %.3f seconds%n",
                    name, (end - start) * 1E-9);
        }
    }
}
