package ribomation.threading;

public class Result {
    long numDirs  = 0;
    long numFiles = 0;
    long numBytes = 0;

    public Result() {
    }

    Result add(Result r) {
        numDirs += r.numDirs;
        numFiles += r.numFiles;
        numBytes += r.numBytes;
        return this;
    }

    @Override
    public String toString() {
        return String.format("# Dirs = %,d%n# Files = %,d%nTot Size = %,.3f MB",
                numDirs, numFiles, numBytes/ (1024 * 1024D));
    }
}
