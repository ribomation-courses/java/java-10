package ribomation.threading;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

public class ComputeSize extends RecursiveTask<Result> {
    private File dir;

    public ComputeSize(File dir) {
        if (!dir.isDirectory()) {
            throw new IllegalArgumentException("not a directory: " + dir.getAbsolutePath());
        }

        this.dir = dir;
    }

    @Override
    protected Result compute() {
        Result r = new Result();
        List<ComputeSize> tasks = new ArrayList<>();

        for (File f : Objects.requireNonNull(dir.listFiles())) {
            if (f.isFile()) {
                r.numFiles++;
                r.numBytes += f.length();
            } else if (f.isDirectory()) {
                r.numDirs++;
                tasks.add((ComputeSize) new ComputeSize(f).fork());
            }
        }

       return tasks.stream()
                .map(ForkJoinTask::join)
                .collect(() -> r,
                        /*Result::add*/ (acc,elem) -> {acc.add(elem);},
                        Result::add);
    }
}
